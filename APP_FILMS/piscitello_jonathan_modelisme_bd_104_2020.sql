-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 02 Juillet 2020 à 10:17
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18


-- Database: piscitello_jonathan_modelisme_bd_104_2020
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists piscitello_jonathan_modelisme_bd_104_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS piscitello_jonathan_modelisme_bd_104_2020;

-- Utilisation de cette base de donnée

USE piscitello_jonathan_modelisme_bd_104_2020;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `piscitello_jonathan_modelisme_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_mail`
--

CREATE TABLE `t_mail` (
  `id_mail` int(11) NOT NULL,
  `adresse_mail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_mail`
--

INSERT INTO `t_mail` (`id_mail`, `adresse_mail`) VALUES
(1, 'Jonathan.piscitello@outlook.fr');

-- --------------------------------------------------------

--
-- Structure de la table `t_mail_personne`
--

CREATE TABLE `t_mail_personne` (
  `id_mail_personne` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL,
  `fk_mail` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_objet`
--

CREATE TABLE `t_objet` (
  `id_objet` int(11) NOT NULL,
  `marque_objet` text NOT NULL,
  `utiliter_objet` text NOT NULL,
  `taille_objet` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_objet`
--

INSERT INTO `t_objet` (`id_objet`, `marque_objet`, `utiliter_objet`, `taille_objet`) VALUES
(1, 'Marklin', 'Trains', '25cm'),
(13, 'HP', 'PC', '24 Pouces');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `id_personne` int(11) NOT NULL,
  `prenom_personne` text NOT NULL,
  `nom_personne` text NOT NULL,
  `age_personne` int(11) DEFAULT NULL,
  `genre_personne` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personne`
--

INSERT INTO `t_personne` (`id_personne`, `prenom_personne`, `nom_personne`, `age_personne`, `genre_personne`) VALUES
(15, 'Jonathan', 'Piscitello', 17, 'Homme'),
(27, 'Maccaud', 'Olivier', 57, 'Homme');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_objet`
--

CREATE TABLE `t_pers_objet` (
  `id_pers_objet` int(11) NOT NULL,
  `fk_objet` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_pers_objet`
--

INSERT INTO `t_pers_objet` (`id_pers_objet`, `fk_objet`, `fk_personne`) VALUES
(11, 1, 15),
(12, 13, 27);

-- --------------------------------------------------------

--
-- Structure de la table `t_telephone`
--

CREATE TABLE `t_telephone` (
  `id_telephone` int(11) NOT NULL,
  `num_tel` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_telephone`
--

INSERT INTO `t_telephone` (`id_telephone`, `num_tel`) VALUES
(1, '079 289 1527');

-- --------------------------------------------------------

--
-- Structure de la table `t_tel_personne`
--

CREATE TABLE `t_tel_personne` (
  `id_tel_personne` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL,
  `fk_telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_mail`
--
ALTER TABLE `t_mail`
  ADD PRIMARY KEY (`id_mail`);

--
-- Index pour la table `t_mail_personne`
--
ALTER TABLE `t_mail_personne`
  ADD PRIMARY KEY (`id_mail_personne`),
  ADD KEY `fk_personne` (`fk_personne`,`fk_mail`),
  ADD KEY `fk_mail` (`fk_mail`);

--
-- Index pour la table `t_objet`
--
ALTER TABLE `t_objet`
  ADD PRIMARY KEY (`id_objet`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`id_personne`);

--
-- Index pour la table `t_pers_objet`
--
ALTER TABLE `t_pers_objet`
  ADD PRIMARY KEY (`id_pers_objet`),
  ADD KEY `fk_personne` (`fk_personne`),
  ADD KEY `fk_objet` (`fk_objet`);

--
-- Index pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
  ADD PRIMARY KEY (`id_telephone`);

--
-- Index pour la table `t_tel_personne`
--
ALTER TABLE `t_tel_personne`
  ADD PRIMARY KEY (`id_tel_personne`),
  ADD KEY `fk_personne` (`fk_personne`,`fk_telephone`),
  ADD KEY `fk_telephone` (`fk_telephone`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_mail`
--
ALTER TABLE `t_mail`
  MODIFY `id_mail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_mail_personne`
--
ALTER TABLE `t_mail_personne`
  MODIFY `id_mail_personne` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_objet`
--
ALTER TABLE `t_objet`
  MODIFY `id_objet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `t_pers_objet`
--
ALTER TABLE `t_pers_objet`
  MODIFY `id_pers_objet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
  MODIFY `id_telephone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_tel_personne`
--
ALTER TABLE `t_tel_personne`
  MODIFY `id_tel_personne` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_mail_personne`
--
ALTER TABLE `t_mail_personne`
  ADD CONSTRAINT `t_mail_personne_ibfk_1` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_personne`),
  ADD CONSTRAINT `t_mail_personne_ibfk_2` FOREIGN KEY (`fk_mail`) REFERENCES `t_mail` (`id_mail`);

--
-- Contraintes pour la table `t_pers_objet`
--
ALTER TABLE `t_pers_objet`
  ADD CONSTRAINT `t_pers_objet__ibfk_1` FOREIGN KEY (`fk_objet`) REFERENCES `t_objet` (`id_objet`),
  ADD CONSTRAINT `t_pers_objet__ibfk_2` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_personne`);

--
-- Contraintes pour la table `t_tel_personne`
--
ALTER TABLE `t_tel_personne`
  ADD CONSTRAINT `t_tel_personne_ibfk_1` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_personne`),
  ADD CONSTRAINT `t_tel_personne_ibfk_2` FOREIGN KEY (`fk_telephone`) REFERENCES `t_telephone` (`id_telephone`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
