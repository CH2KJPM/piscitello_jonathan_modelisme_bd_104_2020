# routes_gestion_telephones.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les telephones.

import pymysql
from flask import render_template, flash, redirect, url_for, request
from APP_FILMS import obj_mon_application
from APP_FILMS.TELEPHONES.data_gestion_telephones import GestionTelephones
from APP_FILMS.DATABASE.erreurs import *
import re


# OM 2020.04.16 Afficher un avertissement sympa...mais contraignant
# Pour la tester http://127.0.0.1:5005/avertissement_sympa_pour_geeks
# possible errreur de Jonathan A VOIR SI PROBLEME changer id_tel_sel/oder_by


# OM 2020.04.16 Afficher les telephones
# Pour la tester http://127.0.0.1:5005/telephones_afficher
@obj_mon_application.route("/telephones_afficher")
def telephones_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephones = GestionTelephones()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionTelephones()
            # Fichier data_gestion_telephones.py
            data_telephones = obj_actions_telephones.telephones_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data telephones", data_telephones, "type ", type(data_telephones))
            # Différencier les messages si la table est vide.
            if data_telephones:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données Téléphones affichées !!", "success")
            else:
                flash("""La table "t_objet" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}", "danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("telephones/telephones_afficher.html", data=data_telephones)


# OM 2020.04.06 Pour une simple démo. On insère deux fois des valeurs dans la table telephones
# Une fois de manière fixe, vous devez changer les valeurs pour voir le résultat dans la table "t_telephones"
# La 2ème il faut entrer la valeur du titre du film par le clavier, il ne doit pas être vide.
# Pour les autres valeurs elles doivent être changées ci-dessous.
# Une des valeurs est "None" ce qui en MySql donne "NULL" pour l'attribut "t_telephones.cover_link_film"
# Pour la tester http://127.0.0.1:5005/telephones_add
@obj_mon_application.route("/telephones_add", methods=['GET', 'POST'])
def telephones_add():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephones = GestionTelephones()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "telephones_add.html"
            num_tel = request.form['num_tel_html']

            print("ok")
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            # if not re.match("[a-zA-Z]"prenom_telephone, name_telephone,age_telephone, film_telephone):
            #
            #     # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
            #     flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
            #           f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")
            #     # On doit afficher à nouveau le formulaire "telephones_add.html" à cause des erreurs de "claviotage"
            #     return render_template("telephones/telephones_add.html")
            # else:
            # Constitution d'un dictionnaire et insertion dans la BD
            valeurs_insertion_dictionnaire = {
                "value_num_tel": num_tel}
            print("CHECK!!!", num_tel)
            obj_actions_telephones.add_telephones_data(valeurs_insertion_dictionnaire)
            print("CHECK OK ", num_tel)
            # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Données insérées !!", "success")
            print(f"Données insérées !!")
            # On va interpréter la "route" 'telephones_afficher', car l'utilisateur
            # doit voir le nouveau film qu'il vient d'insérer. Et on l'affiche de manière
            # à voir le dernier élément inséré.
            return redirect(url_for('telephones_afficher', order_by='DESC', id_tel_sel=0))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")



        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        # except (pymysql.err.OperationalError,
        #         pymysql.ProgrammingError,
        #         pymysql.InternalError,
        #         TypeError) as erreur:
        #     flash(f"Autre erreur {erreur}", "danger")
        #     raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(
                f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("telephones/telephones_add.html")


@obj_mon_application.route('/telephones_edit', methods=['POST', 'GET'])
def telephones_edit():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "telephones_afficher.html"
    if request.method == 'GET':
        try:
            # Récupère la valeur de "id_telephone" du formulaire html "telephones_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_telephone"
            # grâce à la variable "id_telephone_edit_html"
            # <a href="{{ url_for('telephones_edit', id_telephone_edit_html=row.id_telephone) }}">Edit</a>
            id_telephone_edit = request.values['id_telephone_edit_html']

            # Récupère le contenu du champ "prenom_telephone" dans le formulaire HTML "telephonesEdit.html"
            # Pour afficher dans la console la valeur de "id_telephone_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_telephone_edit)
            print("PD")

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_telephone": id_telephone_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephones = GestionTelephones()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_telephones = obj_actions_telephones.edit_telephone_data(valeur_select_dictionnaire)
            print("dataIdfilm ", data_id_telephones, "type ", type(data_id_telephones))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer la telephone !!!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("telephones/telephones_edit.html", data=data_id_telephones)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /telephones_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un film de telephones par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/telephones_update', methods=['POST', 'GET'])
def telephones_update():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "telephones_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du film alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ", request.values)

            # Récupère la valeur de "id_telephone" du formulaire html "telephones_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupère la valeur de "id_telephone"
            # grâce à la variable "id_telephone_edit_html"
            # <a href="{{ url_for('telephones_edit', id_telephone_edit_html=row.id_telephone) }}">Edit</a>
            id_telephone_edit = request.values['id_telephone_edit_html']

            # Récupère le contenu du champ "prenom_telephone" dans le formulaire HTML "telephonesEdit.html"
            num_tel = request.values['edit_num_tel_html']

            valeur_edit_list = [{'id_telephone': id_telephone_edit, 'num_tel': num_tel}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("[0-9]{3} [0-9]{3} [0-9]{4}",
                            num_tel):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "prenom_telephone" dans le formulaire HTML "telephonesEdit.html"
                # prenom_telephone = request.values['name_edit_prenom_telephone_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "danger")

                # On doit afficher à nouveau le formulaire "telephones_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "telephones_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_telephone': 13, 'prenom_telephone': 'philosophique'}]
                valeur_edit_list = [{'id_telephone': id_telephone_edit, 'num_tel': num_tel}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "telephones_edit.html"
                print(valeur_edit_list, "type ..", type(valeur_edit_list))
                return render_template('telephones/telephones_edit.html', data=valeur_edit_list)
                print("OK")
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_telephone": id_telephone_edit, "value_num_tel": num_tel}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_telephones = GestionTelephones()

                # La commande MySql est envoyée à la BD
                data_id_telephones = obj_actions_telephones.update_telephone_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataIdfilm ", data_id_telephones, "type ", type(data_id_telephones))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Valeur telephone modifiée. ", "success")
                # On affiche les telephones avec celui qui vient d'être edité en tête de liste. (DESC)
                return redirect(url_for('telephones_afficher', order_by="ASC", id_tel_sel=id_telephone_edit))

        except (Exception,
                # pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                # pymysql.IntegrityError,
                TypeError) as erreur:
            print(erreur.args[0])
            flash(f"problème telephones ____lllupdate{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_prenom_telephone_html" alors on renvoie le formulaire "EDIT"
    return render_template('telephones/telephones_edit.html', data=valeur_edit_list)


@obj_mon_application.route('/telephones_select_delete', methods=['POST', 'GET'])
def telephones_select_delete():
    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephones = GestionTelephones()
            # OM 2019.04.04 Récupère la valeur de "idfilmDeleteHTML" du formulaire html "telephonesDelete.html"
            id_telephone_delete = request.args.get('id_telephone_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_telephone": id_telephone_delete}

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_telephones = obj_actions_telephones.delete_select_telephone_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur telephones_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur telephones_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('telephones/telephones_delete.html', data=data_id_telephones)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /telephonesUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un film, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/telephones_delete', methods=['POST', 'GET'])
def telephones_delete():
    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_telephones = GestionTelephones()
            # OM 2019.04.02 Récupère la valeur de "id_telephone" du formulaire html "telephonesAfficher.html"
            id_telephone_delete = request.form['id_telephone_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_telephone": id_telephone_delete}

            data_telephones = obj_actions_telephones.delete_telephone_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des telephones des telephones
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les telephones
            return redirect(url_for('telephones_afficher', order_by="ASC", id_tel_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "film" de telephones qui est associé dans "t_telephones_telephones".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer cette telephone !!!', "warning")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !! Cette telephone est associé à des objet dans la t_telephones_telephones !!! : {erreur}")
                # Afficher la liste des telephones des telephones
                return redirect(url_for('telephones_afficher', order_by="ASC", id_tel_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur telephones_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur telephones_delete {erreur.args[0], erreur.args[1]}", "danger")

            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('telephones/telephones_afficher.html', data=data_telephones)
