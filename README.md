--INSTALLATION--
Pour commencer il faut télécharger la dernière version de chaque logiciel:
•	Python pour votre OS
•	UwAmp (Windows) ou Mamp (MacOS) ou Xampp (Linux)
•	PyCharm pour votre OS

--CONFIGURATION--
Maintenant que c'est installé il faut configurer !
1.	Ouvrir PyCharm et cliquer sur "Get from Version Control".
2.	Sur la nouvelle fenêtre qui vient de s'ouvrir, dans "URL:", coller le lien GIT et finalement choisir où installer le projet.
3.	Ouvrir le projet piscitello_jonathan_modelisme_bd_104_2020 dans PyCharm, et configurer l'interpréteur python pour que le projet puisse fonctionner.
4.	Pour configurer l'interpréteur, aller sur "File -> Settings -> Project: piscitello_jonathan_modelisme_bd_104_2020 -> Projet Interpreter" cliquer sur le bouton molette et choisir "Add..".
5.	Sur la nouvelle fenêtre la sélection doit être sur "New environment" et cliquer sur "OK".
6.	Si besoin, une fenêtre demandant si on veut installer les packages du projet venant de "requirements.txt", cliquer sur oui.
Voilà PyCharm est prêt à fonctionner !
Maintenant il faut configurer phpMyAdmin pour que quelque chose s'affiche dans le projet, sinon c'est le vide...
7.	Ouvrir UwAmp ou Mamp ou Xampp et démarrer le serveur mysql si ce n'est pas déjà le cas.
8.	Dans un navigateur aller sur http://localhost/mysql, et se log avec
•	Username = root
•	Password = root
9.	Une fois entré dans phpMyAdmin, cliquer sur la petite maison et sous "Importer".
10.	Cliquer sur "Choisir un fichier", une nouvelle fenêtre s'ouvre, aller sur
"..\ piscitello_jonathan_modelisme_bd_104_2020\APP_FILM \DATABASE\ piscitello_jonathan_modelisme_bd_104_2020.sql"
et finalement cliquer sur "Exécuter".
Trop bien, le fichier sql à été importé !
--AFFICHAGE DU PROJET--
1.	Dans PyCharm, sous
" \run_mon_app.py"
Clique-droit sur "run_mon_app.py", dans le menu déroulant, Exécuter "Run 'run_mon_app'". 2. Dans le run de PyCharm, des ligne de codes défilent, et à la fin une ligne comportant http://127.0.0.1:5005/, cliquer sur celle-ci. 3. Un nouvel onglet dans votre navigateur vient de s'ouvrir, c'est le projet !
